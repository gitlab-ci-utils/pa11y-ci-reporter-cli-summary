# Changelog

## v4.0.1 (2024-11-16)

### Fixed

- Fixed example screenshot image path in README.

## v4.0.0 (2024-09-04)

### Changed

- BREAKING: Deprecated support for Node 21 (end-of-life 2024-06-01) and added
  support for Node 22 (released 2024-04-25). Compatible with all current and
  LTS releases (`^18.12.0 || ^20.9.0 || >=22.0.0`). (#35, #38)
- BREAKING: Removed `pa11y-ci` from `peerDependencies` to facilitate use with
  forks. (#39)
  - A `supports` property was added to the reporter to indicate `pa11y-ci`
    compatibility, similar to the `pa11y` reporter interface (although not
    formally part of the `pa11y-ci` reporter interface).

### Fixed

- Removed unneeded development dependency `strip-ansi`. (#8)

### Miscellaneous

- Updated documentation to clarify requirements on `pa11y-ci` and reporter
  installation location. (#32)
- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#37)

## v3.0.0 (2023-06-10)

### Changed

- BREAKING: Deprecated support for Node 14 (end-of-life 2023-04-30) and 19
  (end-of-life 2023-06-01). Compatible with all current and LTS releases
  (^16.13.0 || ^18.12.0 || >=20.0.0). (#31)

## v2.0.2 (2022-11-03)

### Fixed

- Updated devDependency `serve-handler@6.1.5`, resolving [CVE-2022-3517](https://nvd.nist.gov/vuln/detail/CVE-2022-3517). (#28)

## v2.0.1 (2022-08-07)

### Fixed

- Updated to latest dependencies (`kleur@4.1.5`)

## v2.0.0 (2022-06-05)

### Changed

- BREAKING: Deprecated support for Node 12 and 17 since end-of-life. Compatible with all current and LTS releases (`^14.15.0 || ^16.13.0 || >=18.0.0`). (#21, #22)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Added `prettier` and disabled `eslint` formatting rules. (#20)
- Added test coverage threshold requirements (#23)
- Updated CI pipeline to allow failure for Windows tests as temp workaround to [`npm` error](https://github.com/npm/cli/issues/4980) causing Windows tests to fail. (#24)

## v1.1.0 (2022-03-24)

### Changed

- Updated Header and Totals lines to output appropriate plurality (e.g. "1 error", "2 errors") (#11)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities (CVE-2022-0235 and CVE-2021-44906, both dev only)
- Fixed integration tests to properly render color output in all environments (#16, #19)

### Miscellaneous

- Refactored tests to leverage [`pa11y-ci-reporter-runner`](https://www.npmjs.com/package/pa11y-ci-reporter-runner) (#17)
- Refactored tests to eliminate unnecessary ANSI escape code removal (#18)
- Updated project policy documents (CODE_OF_CONDUCT.md, CONTRIBUTING.md, SECURITY.md) (#15)

## v1.0.1 (2021-12-30)

### Fixed

- Updated output to truncate URLs at the max length, which was causing improper results alignment in some cases (#10)
- Updated output to remove URL from puppeteer page errors (e.g. "net::ERR_NAME_NOT_RESOLVED at https://this.url.does.not.exist/") since duplicate (#12)
- Fixed bug where URL objects in pa11y-ci config were not being checked to determine URL max length formatting (#13)

## v1.0.0 (2021-12-27)

Initial release
