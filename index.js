'use strict';

/**
 * The Pa11y CI HTML Reporter.
 *
 * @module pa11y-ci-reporter-html
 */

const formatterFactory = require('./lib/formatter');
const defaultOptions = require('./lib/default-options');

/**
 * Identify compatible versions of pa11y-ci, even though not formally part of
 * the pa11y-ci reporter interface.
 */
const supports = '^3.0.0 || ^4.0.0';

const getTypeCount = (issues, type) =>
    issues.filter((result) => result.type === type).length;

const getIssueSummary = (results) => ({
    errors: getTypeCount(results.issues, 'error'),
    notices: getTypeCount(results.issues, 'notice'),
    warnings: getTypeCount(results.issues, 'warning')
});

/**
 * Factory function that returns a pa11y-ci HTML reporter, generating
 * a summary HTML report and detailed page HTML reports.
 *
 * @param   {object} options The reporter options from configuration.
 * @returns {object}         The pa11y-ci reporter.
 * @static
 */
// eslint-disable-next-line max-lines-per-function -- factory function with state
const reporterFactory = (options = {}) => {
    const currentOptions = { ...defaultOptions, ...options };

    const runResults = {
        errors: 0,
        notices: 0,
        pageErrors: 0,
        warnings: 0
    };

    const addResults = (issues) => {
        runResults.errors += issues.errors;
        runResults.warnings += issues.warnings;
        runResults.notices += issues.notices;
    };

    let formatError, formatHeader, formatResult, formatSummary, formatTitle;

    const beforeAll = (urls) => {
        // eslint-disable-next-line unicorn/no-array-reduce -- intuitive use of reduce
        const maxUrlLength = urls.reduce((accumulator, value) => {
            const url = typeof value === 'string' ? value : value.url;
            return Math.max(accumulator, url.length);
        }, 0);
        ({
            formatError,
            formatHeader,
            formatResult,
            formatSummary,
            formatTitle
        } = formatterFactory(maxUrlLength));
        console.log(formatHeader(urls.length));
        console.log(
            formatTitle(formatResult('URL', 'Errors', 'Warnings', 'Notices'))
        );
    };

    // eslint-disable-next-line no-shadow -- using API nomenclature
    const results = (results) => {
        const issueSummary = getIssueSummary(results);
        addResults(issueSummary);
        console.log(
            formatResult(
                results.pageUrl,
                issueSummary.errors,
                issueSummary.warnings,
                issueSummary.notices
            )
        );
    };

    // eslint-disable-next-line no-shadow -- using API nomenclature
    const error = (error, url) => {
        if (currentOptions.showPageErrors) {
            console.log(formatError(url, error));
        }
        runResults.pageErrors += 1;
    };

    const afterAll = () => {
        console.log(formatSummary(runResults));
    };

    return {
        afterAll,
        beforeAll,
        error,
        results,
        supports
    };
};

module.exports = reporterFactory;
