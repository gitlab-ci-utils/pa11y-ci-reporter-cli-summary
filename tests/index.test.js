'use strict';

const { createRunner, RunnerStates } = require('pa11y-ci-reporter-runner');
const stripAnsi = require('./helpers/strip-ansi');
const reporterFactory = require('../');

const pa11yciResults = require('./fixtures/results/pa11yci-results');
const pa11yciConfigStringObject = require('./fixtures/configs/test-urls-string-object.pa11yc1.json');
const pa11yciConfigAllString = require('./fixtures/configs/test-urls-all-string.pa11yc1.json');

describe('reporter module', () => {
    it('should export a factory function that returns a reporter', () => {
        expect.assertions(1);
        expect(typeof reporterFactory).toBe('function');
    });

    describe('reporter interface', () => {
        let reporter;

        beforeAll(() => {
            reporter = reporterFactory();
        });

        it('should include an afterAll function', () => {
            expect.assertions(1);
            expect(typeof reporter.afterAll).toBe('function');
        });

        it('should include a beforeAll function', () => {
            expect.assertions(1);
            expect(typeof reporter.beforeAll).toBe('function');
        });

        it('should include an error function', () => {
            expect.assertions(1);
            expect(typeof reporter.error).toBe('function');
        });

        it('should include a results function', () => {
            expect.assertions(1);
            expect(typeof reporter.results).toBe('function');
        });
    });
});

describe('cli summary reporter', () => {
    const pa11yciResultsFileName =
        './tests/fixtures/results/pa11yci-results.json';
    const pa11yciResultsNoUrlsFileName =
        './tests/fixtures/results/pa11yci-results-no-urls.json';
    const reporterName = '.';
    const urlWithIssues = 'somewhere-with-issues.html';
    const urlNoIssues = 'somewhere-no-issues.html';
    const urlPageError = 'somewhere-error.html';
    const optionsShowPageErrors = { showPageErrors: false };
    const pageErrorString = 'This is an error';
    const urls = Object.keys(pa11yciResults.results);
    let consoleSpy;

    // Create common reporter runners used for most tests.
    // Unique runners are created per test if required.
    const runnerWithPageErrors = createRunner(
        pa11yciResultsFileName,
        reporterName,
        {},
        pa11yciConfigStringObject
    );
    const runnerNoPageErrors = createRunner(
        pa11yciResultsFileName,
        reporterName,
        optionsShowPageErrors,
        pa11yciConfigStringObject
    );

    /**
     * Common function to run the given reporter to capture page
     * error console output.
     *
     * @param {object} runner Reporter runner.
     * @private
     */
    const runPageErrorWithLog = async (runner) => {
        // Calling beforeAll is required to initialize reporter,
        // then run until the next URL is a page error.
        await runner.runUntilNext(RunnerStates.beginUrl, urlPageError);
        // Clear mock so only console output is from error
        consoleSpy.mockClear();
        // Run page error
        await runner.runUntil(RunnerStates.urlResults, urlPageError);
    };

    /**
     * Common function to run the given reporter to capture
     * afterAll console output.
     *
     * @param {object} runner Reporter runner.
     * @private
     */
    const runAfterAllWithLog = async (runner) => {
        // Calling beforeAll is required to initialize reporter,
        // then run until the next URL is a page error.
        await runner.runUntilNext(RunnerStates.afterAll);
        // Clear mock so only console output is from afterAll
        consoleSpy.mockClear();
        // Run page error
        await runner.runUntil(RunnerStates.afterAll);
    };

    beforeAll(() => {
        consoleSpy = jest.spyOn(console, 'log').mockImplementation(() => {});
    });

    afterAll(() => {
        jest.restoreAllMocks();
    });

    describe('options', () => {
        beforeEach(async () => {
            // Clear console.log mock since tests rely on output
            consoleSpy.mockClear();
            await runnerWithPageErrors.reset();
            await runnerNoPageErrors.reset();
        });

        it('should use showPageErrors from options if provided', async () => {
            expect.hasAssertions();
            await runPageErrorWithLog(runnerNoPageErrors);
            expect(consoleSpy).not.toHaveBeenCalled();
        });

        it('should use default showPageErrors if not provided in options', async () => {
            expect.hasAssertions();
            await runPageErrorWithLog(runnerWithPageErrors);
            expect(consoleSpy).toHaveBeenCalledTimes(1);
        });
    });

    describe('reporter', () => {
        describe('set max url length', () => {
            beforeEach(() => {
                // Clear console.log mock since tests rely on output
                consoleSpy.mockClear();
            });

            const maxUrlLengthCases = [
                {
                    pa11yciConfig: pa11yciConfigAllString,
                    testSuffix: 'all strings'
                },
                {
                    pa11yciConfig: pa11yciConfigStringObject,
                    testSuffix: 'mix of strings and objects'
                }
            ];

            it.each(maxUrlLengthCases)(
                'should set max url length if url array is $testSuffix',
                async ({ pa11yciConfig }) => {
                    expect.assertions(1);
                    const padding = 10;

                    // Create unique runner since testing different pa11y-ci configs
                    const runner = createRunner(
                        pa11yciResultsFileName,
                        reporterName,
                        {},
                        pa11yciConfig
                    );
                    // Check page error URL since not the longest, so should have to be extended
                    await runPageErrorWithLog(runner);

                    // With ANSI escape codes, end of whitespace after URL is the start of error formatting
                    const match = consoleSpy.mock.calls[0][0].match(
                        new RegExp(`(${urlPageError}\\s+).*$`)
                    );
                    // UrlWithIssues is the longest for all configs, so use with padding as expected length
                    expect(match[1]).toHaveLength(
                        urlWithIssues.length + padding
                    );
                }
            );
        });

        describe('reporter functions', () => {
            describe('beforeAll', () => {
                beforeAll(async () => {
                    // Clear console.log mock since tests rely on output
                    consoleSpy.mockClear();
                    await runnerNoPageErrors.reset();
                    await runnerNoPageErrors.runUntil(RunnerStates.beforeAll);
                });

                it('should display run summary', () => {
                    expect.assertions(1);
                    expect(consoleSpy.mock.calls[0][0]).toMatch(
                        `${urls.length} URLs`
                    );
                });

                it('should display result header', () => {
                    expect.assertions(1);
                    expect(consoleSpy.mock.calls[1][0]).toMatch(
                        /URL.*Errors.*Warnings.*Notices/
                    );
                });
            });

            describe('results', () => {
                beforeEach(async () => {
                    // Clear console.log mock since tests rely on output
                    consoleSpy.mockClear();
                    await runnerWithPageErrors.reset();
                });

                const resultsCases = [
                    {
                        expectedCounts: { errors: 3, notices: 1, warnings: 2 },
                        testName: 'with issues',
                        url: urlWithIssues
                    },
                    {
                        expectedCounts: { errors: 0, notices: 0, warnings: 0 },
                        testName: 'with no issues',
                        url: urlNoIssues
                    }
                ];

                it.each(resultsCases)(
                    'should display page results $testName',
                    async ({ url, expectedCounts }) => {
                        expect.assertions(1);
                        // Calling beforeAll is required to initialize reporter,
                        // then run up until url being tested is next
                        await runnerWithPageErrors.runUntilNext(
                            RunnerStates.beginUrl,
                            url
                        );
                        // Reset mock here so only console output is from results
                        consoleSpy.mockClear();

                        await runnerWithPageErrors.runUntil(
                            RunnerStates.urlResults,
                            url
                        );

                        // If ANSI escape codes are stripped, can check against a
                        // meaningful regex. Otherwise the regex becomes either
                        // incredibly complex or meaningless.
                        const output = stripAnsi(consoleSpy.mock.calls[0][0]);
                        expect(output).toMatch(
                            new RegExp(
                                `${url}\\s*${expectedCounts.errors}\\s*${expectedCounts.warnings}\\s*${expectedCounts.notices}`
                            )
                        );
                    }
                );
            });

            describe('error', () => {
                beforeEach(async () => {
                    // Clear console.log mock since tests rely on output
                    consoleSpy.mockClear();
                    await runnerWithPageErrors.reset();
                    await runnerNoPageErrors.reset();
                });

                it('should display page errors if showPageErrors is true', async () => {
                    expect.assertions(1);
                    // Default runner has showPageErrors = true
                    await runPageErrorWithLog(runnerWithPageErrors);
                    expect(consoleSpy.mock.calls[0][0]).toMatch(
                        new RegExp(`${urlPageError}.*${pageErrorString}`)
                    );
                });

                it('should not display page errors if showPageErrors is false', async () => {
                    expect.assertions(1);
                    // Options runner has showPageErrors = false
                    await runPageErrorWithLog(runnerNoPageErrors);
                    expect(consoleSpy).not.toHaveBeenCalled();
                });
            });

            describe('afterAll', () => {
                beforeEach(async () => {
                    // Clear console.log mock since tests rely on output
                    consoleSpy.mockClear();
                    await runnerWithPageErrors.reset();
                    await runnerNoPageErrors.reset();
                });

                const afterAllCountTestCases = [
                    {
                        expectedCounts: {
                            errors: '0 errors',
                            notices: '0 notices',
                            pageErrors: '0 page errors',
                            warnings: '0 warnings'
                        },
                        runner: createRunner(
                            pa11yciResultsNoUrlsFileName,
                            reporterName
                        ),
                        testName: 'zero counts if no pages tested'
                    },
                    {
                        expectedCounts: {
                            errors: '3 errors',
                            notices: '1 notice',
                            pageErrors: '1 page error',
                            warnings: '2 warnings'
                        },
                        runner: runnerWithPageErrors,
                        testName: 'correct counts if pages tested'
                    }
                ];

                it.each(afterAllCountTestCases)(
                    'should display run summary with $testName',
                    async ({ expectedCounts, runner }) => {
                        expect.assertions(4);

                        await runAfterAllWithLog(runner);

                        const [[output]] = consoleSpy.mock.calls;
                        expect(output).toContain(expectedCounts.errors);
                        expect(output).toContain(expectedCounts.warnings);
                        expect(output).toContain(expectedCounts.notices);
                        /* eslint-disable-next-line jest/no-conditional-in-test -- avoid duplicate test logic */
                        /0 page errors/.test(expectedCounts.pageErrors)
                            ? /* eslint-disable-next-line jest/no-conditional-expect -- simplify tests */
                              expect(output).not.toContain(
                                  expectedCounts.pageErrors
                              )
                            : /* eslint-disable-next-line jest/no-conditional-expect -- simplify tests */
                              expect(output).toContain(
                                  expectedCounts.pageErrors
                              );
                    }
                );

                const afterAllSummaryTestCases = [
                    {
                        runner: runnerNoPageErrors,
                        testName: 'showPageErrors is false'
                    },
                    {
                        runner: runnerWithPageErrors,
                        testName: 'showPageErrors is true'
                    }
                ];

                it.each(afterAllSummaryTestCases)(
                    'should display run summary with page errors if $testName',
                    async ({ runner }) => {
                        expect.assertions(1);
                        await runAfterAllWithLog(runner);
                        expect(consoleSpy.mock.calls[0][0]).toContain(
                            'page error'
                        );
                    }
                );
            });
        });
    });
});
