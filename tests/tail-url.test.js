'use strict';

const tailUrl = require('../lib/tail-url');

const maxLength = 25;

describe('tail-url', () => {
    it('should return the original url if shorter than max length', () => {
        expect.assertions(1);
        const url = 'https://somewhere.org';

        const result = tailUrl(url, maxLength);

        expect(result).toBe(url);
    });

    it('should split urls on /', () => {
        expect.assertions(1);
        const url = 'https://somewhere.org/john/paul/george/ringo';
        const expectedResult = '…/john/paul/george/ringo';

        const result = tailUrl(url, maxLength);

        expect(result).toBe(expectedResult);
    });

    it('should split urls on ?', () => {
        expect.assertions(1);
        const url = 'https://somewhere.org/foo?john=paul&george=ringo';
        const expectedResult = '…?john=paul&george=ringo';

        const result = tailUrl(url, maxLength);

        expect(result).toBe(expectedResult);
    });

    it('should split urls on #', () => {
        expect.assertions(1);
        const url = 'https://somewhere.org/foo#john-paul-george-ringo';
        const expectedResult = '…#john-paul-george-ringo';

        const result = tailUrl(url, maxLength);

        expect(result).toBe(expectedResult);
    });

    it('should include the splitting character if at the max length', () => {
        expect.assertions(1);
        const url = 'https://somewhere.org/john/paul/george/ringos';
        const expectedResult = '…/john/paul/george/ringos';

        const result = tailUrl(url, maxLength);

        expect(result).toBe(expectedResult);
    });

    it('should not include the splitting character if outside the max length', () => {
        expect.assertions(1);
        const url = 'https://somewhere.org/johns/paul/george/ringos';
        const expectedResult = '…johns/paul/george/ringos';

        const result = tailUrl(url, maxLength);

        expect(result).toBe(expectedResult);
    });

    it('should prepend the url with an ellipsis if shortened', () => {
        expect.assertions(1);
        const url = 'https://somewhere.org/john/paul/george/ringo';

        const result = tailUrl(url, maxLength);

        expect(result[0]).toBe('…');
    });

    it('should return a truncated url if a logical boundary cannot be found within the max length', () => {
        expect.assertions(1);
        const url = 'https://this.is.a.long.url.without.a.logical.boundary';
        const expectedUrl = '…thout.a.logical.boundary';

        const result = tailUrl(url, maxLength);

        expect(result).toBe(expectedUrl);
    });
});
