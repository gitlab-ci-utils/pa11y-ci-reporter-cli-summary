'use strict';

const fs = require('node:fs');
const path = require('node:path');
const { promisify } = require('node:util');
const exec = promisify(require('node:child_process').exec);
const { startServer } = require('./helpers/http-server');

const testConfigOptionsFileName =
    './tests/fixtures/configs/test-with-options.pa11yci.json';
const testConfigNoOptionsFileName =
    './tests/fixtures/configs/test-no-options.pa11yci.json';

const nodeMajorVersion = Number.parseInt(
    process.version.split('.')[0].replace('v', '')
);

// Get command to execute package CLI command. Since yarn PnP doesn't
// use node_modules npx raises a warning and the test fails, so ensure
// it's run with yarn run.
const getExecCommand = () => {
    if (fs.existsSync(path.resolve(process.cwd(), '.pnp.cjs'))) {
        return 'yarn run';
    }
    return 'npx';
};

// Longer timeout needed when generating report (default 5s).
// Increased to 120s since 60s was timing out for Windows CI jobs.
const reportTimeout = 120_000;
jest.setTimeout(reportTimeout);

const testReporterWithOptions = async (command) => {
    expect.assertions(nodeMajorVersion < 21 ? 2 : 1);

    // Spawn process to execute command to generate report
    const { stdout, stderr } = await exec(command, {
        env: { ...process.env, FORCE_COLOR: 1 }
    });

    // Starting with Node 21, get the error "[DEP0040] DeprecationWarning:
    // The `punycode` module is deprecated. Please use a userland alternative
    // instead".
    if (nodeMajorVersion < 21) {
        expect(stderr).toBe('');
    }
    expect(stdout).toMatchSnapshot();
};

describe('pa11y-ci cli summary reporter', () => {
    // eslint-disable-next-line jest/no-done-callback -- startServer API
    beforeAll((done) => {
        // eslint-disable-next-line promise/prefer-await-to-callbacks -- startServer API
        startServer((error, server) => {
            if (!error) {
                globalThis.server = server;
            }
            done(error);
        });
    });

    afterAll(() => {
        if (globalThis.server) {
            globalThis.server.close();
        }
    });

    it('should create report with default options if reporter specified in CLI', async () => {
        expect.hasAssertions();
        const command = `${getExecCommand()} pa11y-ci -c ${testConfigOptionsFileName} --reporter "./index.js" && exit 1 || exit 0`;
        await testReporterWithOptions(command);
    });

    it('should create report with default options if none specified in config', async () => {
        expect.hasAssertions();
        const command = `${getExecCommand()} pa11y-ci -c ${testConfigNoOptionsFileName} && exit 1 || exit 0`;
        await testReporterWithOptions(command);
    });

    it('should create report with options specified in config', async () => {
        expect.hasAssertions();
        const command = `${getExecCommand()} pa11y-ci -c ${testConfigOptionsFileName} && exit 1 || exit 0`;
        await testReporterWithOptions(command);
    });
});
