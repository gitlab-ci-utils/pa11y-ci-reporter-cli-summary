'use strict';

/**
 * Create an http-server programmatically.
 *
 * @module http-server
 */

const http = require('node:http');
const handler = require('serve-handler');

const port = 8080;
const options = {
    // Root directory of server, relative to cwd for tests
    public: 'tests/fixtures/site'
};

/**
 * Creates an http-server listening on port 8080,
 * serving the test site, and returns a reference.
 *
 * @param {Function} done Callback function.
 */
const startServer = (done) => {
    const server = http.createServer((request, response) =>
        handler(request, response, options)
    );

    // eslint-disable-next-line promise/prefer-await-to-callbacks -- http API
    server.listen(port, (error) => {
        done(error, server);
    });
};

module.exports.startServer = startServer;
