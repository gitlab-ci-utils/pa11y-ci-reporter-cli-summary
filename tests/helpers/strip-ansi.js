'use strict';

/**
 * Strip ANSI formatting characters from a string.
 *
 * @param   {string} value The string to be stripped of ANSI formatting characters.
 * @returns {string}       The string without ANSI formatting characters.
 * @static
 * @public
 */
const stripAnsi = (value) => {
    // Pattern based on https://stackoverflow.com/questions/25245716/remove-all-ansi-colors-styles-from-strings#answer-29497680
    const pattern =
        '[\u001B\u009B][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]';
    return value.replaceAll(new RegExp(pattern, 'g'), '');
};

module.exports = stripAnsi;
