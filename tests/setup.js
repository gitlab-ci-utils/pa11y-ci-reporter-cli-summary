'use strict';

// Forces ansi escape codes to render even if not TTY.
// Ensure tests run consistently in all environments.
process.env.FORCE_COLOR = 1;
