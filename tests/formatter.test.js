'use strict';

const { bold, red, yellow, cyan } = require('kleur');
const stripAnsi = require('./helpers/strip-ansi');
const formatterFactory = require('../lib/formatter');

describe('formatter factory', () => {
    it('should return a factory function', () => {
        expect.assertions(1);
        expect(typeof formatterFactory).toBe('function');
    });
});

describe('formatter', () => {
    let formatter;
    const maxUrlLength = 42;
    const url = 'http://pa11y.org/';

    const pageError = new Error('This is a test');
    const reError = new RegExp(
        `^(\\s*)(${url}\\s*)(.*${pageError.message}.*)$`,
        's'
    );
    const reErrorMatches = 4;

    // For these tests, variables with 1/2/3 in the name indicate
    // the number of digits in the value(s)
    const result1 = { errors: 7, notices: 9, warnings: 8 };
    const result2 = { errors: 34, notices: 78, warnings: 56 };
    const result3 = { errors: 123, notices: 789, warnings: 456 };
    const maxErrorsDigits = 12_345;
    const getReResult = (results) =>
        new RegExp(
            `^(\\s*)(${url}\\s*)(.*${results.errors}.*)(.*${results.warnings}.*)(.*${results.notices}.*)$`,
            's'
        );
    const reResultMatches = 6;

    const pageErrors = 56;
    const reSummary = new RegExp(
        `^.+${result2.errors} errors.+${result2.warnings} warnings.+${result2.notices} notices(.+${pageErrors} page errors)*.*$`,
        's'
    );
    const reSummaryMatches = 2;

    beforeAll(() => {
        formatter = formatterFactory(maxUrlLength);
    });

    it('should align start and end of URLs for results and errors', () => {
        expect.assertions(2);
        const errorString = formatter.formatError(url, pageError);
        const errorMatches = errorString.match(reError);
        // Use maxErrorsDigits, which has the maximum number of digits for errors. Otherwise,
        // the errors will have leading space padding and the alignment won't match.
        const resultString = formatter.formatResult(
            url,
            maxErrorsDigits,
            result2.warnings,
            result2.notices
        );
        const resultMatches = resultString.match(getReResult(result2));

        // The first capture group is the spacing before the url. The second
        // capture group is the end of whitespace after the URL.
        expect(errorMatches[1]).toHaveLength(resultMatches[1].length);
        expect(errorMatches[2]).toHaveLength(resultMatches[2].length);
    });

    it('should throw if maxUrlLength is not a number', () => {
        expect.assertions(1);
        expect(() => formatterFactory('foo')).toThrow('maxUrlLength');
    });

    const testLimitMaxUrlLength = (maxLength, longUrl, shortenedUrl) => {
        expect.assertions(1);
        const limitedFormatter = formatterFactory(maxLength);

        const results = limitedFormatter.formatResult(
            longUrl,
            pageError.message
        );

        expect(results).toMatch(shortenedUrl);
    };

    it('should limit the maxUrlLength to 100 if max > 100', () => {
        expect.hasAssertions();
        const maxLength = 152;
        const longUrl =
            'https://somewhere.org/this/is/a/really/really/really/' +
            'really/really/really/really/really/really/really/really/really/' +
            'really/really/really/really/long/url';
        const shortenedUrl =
            '…really/really/really/really/really/really/' +
            'really/really/really/really/really/really/really/long/url';
        testLimitMaxUrlLength(maxLength, longUrl, shortenedUrl);
    });

    it('should limit the maxUrlLength to 100 if max + padding (6) > 100', () => {
        expect.hasAssertions();
        const maxLength = 96;
        const longUrl =
            'https://somewhere.org/this/is/also/a/really/really/' +
            'really/really/really/really/really/really/really/really/really/long/url';
        // If not limited, max would be 102 and "this/" would be included
        const shortenedUrl =
            '…/is/also/a/really/really/really/really/really/really/' +
            'really/really/really/really/really/long/url';
        testLimitMaxUrlLength(maxLength, longUrl, shortenedUrl);
    });

    describe('formatError', () => {
        let matches;

        beforeAll(() => {
            const result = formatter.formatError(url, pageError);
            matches = result.match(reError);
        });

        it('should return formatted URL error', () => {
            expect.assertions(1);
            expect(matches).toHaveLength(reErrorMatches);
        });

        it('should color error message red', () => {
            expect.assertions(1);
            expect(matches[0]).toMatch(red(pageError.message));
        });

        it('should extract error message from puppeteer formatted error', () => {
            expect.assertions(2);
            const puppeteerMessage = 'net::ERR_NAME_NOT_RESOLVED';
            const puppeteerError = new Error(`${puppeteerMessage} at ${url}`);
            const expectedStrippedMessage = `at ${url}`;

            const result = formatter.formatError(url, puppeteerError);

            expect(result).toContain(puppeteerMessage);
            expect(result).not.toContain(expectedStrippedMessage);
        });

        it('should trim URLs that exceed max length', () => {
            expect.assertions(1);
            const longUrl =
                'https://somewhere.org/this/is/a/really/really/really/really/long/url';
            // Expect shortened to 48 characters (42 maxLength + 6 padding)
            const shortenedUrl =
                '…/this/is/a/really/really/really/really/long/url';
            const expectedResultMatch = new RegExp(`^\\s*${shortenedUrl}\\s*`);

            const results = formatter.formatError(longUrl, pageError);

            expect(results).toMatch(expectedResultMatch);
        });
    });

    describe('formatHeader', () => {
        const formatHeaderCases = [
            {
                count: 0,
                expectedResult: '\nAnalyzing 0 URLs:',
                testName: 'with 0 URLs'
            },
            {
                count: 1,
                expectedResult: '\nAnalyzing 1 URL:',
                testName: 'with 1 URL'
            },
            {
                count: 10,
                expectedResult: '\nAnalyzing 10 URLs:',
                testName: 'with more than 1 URL'
            }
        ];

        it.each(formatHeaderCases)(
            'should return correct header $testName',
            ({ count, expectedResult }) => {
                expect.assertions(1);
                expect(formatter.formatHeader(count)).toBe(expectedResult);
            }
        );
    });

    describe('formatResult', () => {
        let matches1;
        let matches2;
        let matches3;

        beforeAll(() => {
            const result1String = formatter.formatResult(
                url,
                result1.errors,
                result1.warnings,
                result1.notices
            );
            matches1 = result1String.match(getReResult(result1));

            const result2String = formatter.formatResult(
                url,
                result2.errors,
                result2.warnings,
                result2.notices
            );
            matches2 = result2String.match(getReResult(result2));

            const result3String = formatter.formatResult(
                url,
                result3.errors,
                result3.warnings,
                result3.notices
            );
            matches3 = result3String.match(getReResult(result3));
        });

        it('should return formatted URL result', () => {
            expect.assertions(1);
            expect(matches2).toHaveLength(reResultMatches);
        });

        const checkRightAlignment = (value1, value2, value3) => {
            expect.assertions(2);
            // Strip ANSI escape codes to check visual alignment.
            // 1/2/3 indicate number of digits in the value
            const result1Start = stripAnsi(matches1[0]).indexOf(value1);
            const result2Start = stripAnsi(matches2[0]).indexOf(value2);
            const result3Start = stripAnsi(matches3[0]).indexOf(value3);

            // For right alignment, the start of "1" should be 1 more than "2",
            // and the start of "2" should be one more than "3".
            expect(result1Start - 1).toBe(result2Start);
            expect(result2Start - 1).toBe(result3Start);
        };

        it('should right align errors', () => {
            expect.hasAssertions();
            checkRightAlignment(result1.errors, result2.errors, result3.errors);
        });

        it('should right align warnings', () => {
            expect.hasAssertions();
            checkRightAlignment(
                result1.warnings,
                result2.warnings,
                result3.warnings
            );
        });

        it('should right align notices', () => {
            expect.hasAssertions();
            checkRightAlignment(
                result1.notices,
                result2.notices,
                result3.notices
            );
        });

        it('should color errors red if numeric', () => {
            expect.assertions(1);
            // Strip whitespace because counts are padded for alignment
            // before adding color, so colored text include the whitespace.
            expect(matches2[0].replaceAll(/\s/g, '')).toMatch(
                red(result2.errors)
            );
        });

        it('should color warnings yellow if numeric', () => {
            expect.assertions(1);
            // Strip whitespace because counts are padded for alignment
            // before adding color, so colored text include the whitespace.
            expect(matches2[0].replaceAll(/\s/g, '')).toMatch(
                yellow(result2.warnings)
            );
        });

        it('should color notices cyan if numeric', () => {
            expect.assertions(1);
            // Strip whitespace because counts are padded for alignment
            // before adding color, so colored text include the whitespace.
            expect(matches2[0].replaceAll(/\s/g, '')).toMatch(
                cyan(result2.notices)
            );
        });

        it('should not color errors/warnings/notices if not numeric', () => {
            expect.assertions(1);
            const result = formatter.formatResult(
                url,
                'errors',
                'warnings',
                'notices'
            );
            expect(stripAnsi(result)).toBe(result);
        });

        it('should remove trailing whitespace', () => {
            expect.assertions(1);
            const result = formatter.formatResult(
                url,
                result2.errors,
                result2.warnings,
                result2.notices
            );
            // Strip ANSI escape codes to look purely at alignment
            expect(stripAnsi(result)).toMatch(
                new RegExp(`${result2.notices}$`)
            );
        });

        it('should trim URLs that exceed max length', () => {
            expect.assertions(1);
            const longUrl =
                'https://somewhere.org/this/is/a/really/really/really/really/long/url';
            // Expect shortened to 48 characters (42 maxLength + 6 padding)
            const shortenedUrl =
                '…/this/is/a/really/really/really/really/long/url';
            const { errors, warnings, notices } = result2;
            const expectedResultMatch = new RegExp(`^\\s*${shortenedUrl}\\s*`);

            const results = formatter.formatResult(
                longUrl,
                errors,
                warnings,
                notices
            );

            expect(results).toMatch(expectedResultMatch);
        });
    });

    describe('formatSummary', () => {
        let matchesWithError;
        let matchesNoError;

        beforeAll(() => {
            const resultWithError = formatter.formatSummary({
                errors: result2.errors,
                notices: result2.notices,
                pageErrors,
                warnings: result2.warnings
            });
            matchesWithError = resultWithError.match(reSummary);

            const resultNoError = formatter.formatSummary({
                errors: result2.errors,
                notices: result2.notices,
                warnings: result2.warnings
            });
            matchesNoError = resultNoError.match(reSummary);
        });

        it('should return formatted summary with page errors', () => {
            expect.assertions(1);
            expect(matchesWithError).toHaveLength(reSummaryMatches);
        });

        it('should return formatted summary without page errors', () => {
            expect.assertions(1);
            expect(matchesNoError).toHaveLength(reSummaryMatches);
        });

        it('should not show page errors if none provided', () => {
            expect.assertions(1);
            expect(matchesNoError[0]).not.toContain('page errors');
        });

        it('should color errors red', () => {
            expect.assertions(1);
            expect(matchesWithError[0]).toMatch(
                red(`${result2.errors} errors`)
            );
        });

        it('should color warnings yellow', () => {
            expect.assertions(1);
            expect(matchesWithError[0]).toMatch(
                yellow(`${result2.warnings} warnings`)
            );
        });

        it('should color notices cyan', () => {
            expect.assertions(1);
            expect(matchesWithError[0]).toMatch(
                cyan(`${result2.notices} notices`)
            );
        });

        it('should color page errors red', () => {
            expect.assertions(1);
            expect(matchesWithError[0]).toMatch(
                red(`${pageErrors} page errors`)
            );
        });

        it('should use single plurality if one of each type', () => {
            expect.assertions(1);
            const resultOneEach = formatter.formatSummary({
                errors: 1,
                notices: 1,
                pageErrors: 1,
                warnings: 1
            });

            // Strip ANSI escape codes since only testing text
            expect(stripAnsi(resultOneEach)).toMatch(
                /\b1 error, 1 warning, 1 notice, 1 page error\b/
            );
        });
    });

    describe('formatTitle', () => {
        it('should bold title', () => {
            expect.assertions(1);
            const title = 'Test Title';

            const result = formatter.formatTitle(title);
            expect(result).toMatch(bold(title));
        });
    });
});
