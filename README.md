# Pa11y CI CLI Summary Reporter

Pa11y CI CLI Summary Reporter outputs a summary of
[Pa11y CI](https://github.com/pa11y/pa11y-ci) results to `stdout` - only the
URL and a count of the errors/warning/notices for each page - versus the
complete results that the built-in CLI reporter outputs. An example screenshot
is shown below.

![screenshot of Pa11y CI CLI Summary Reporter output](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-cli-summary/-/raw/main/docs/screenshot.png)

As shown in the preceding example, in some cases the reported URL is longer
than specified for the pa11y-ci analysis, which is used to set the URL column
width, so in that case the URL is truncated and the trailing portion is
displayed.

## Installation

Install Pa11y CI CLI Summary Reporter via
[npm](https://www.npmjs.com/package/pa11y-ci-reporter-cli-summary).

```sh
npm install pa11y-ci-reporter-cli-summary
```

Note: `pa11y-ci` and `pa11y-ci-reporter-cli-summary` must either both be
installed globally or both be installed locally. Using a mixed configuration
can result in the reporter not being found.

## Usage

Pa11y CI CLI Summary Reporter is a Pa11y CI compatible reporter. As with all
reporters, it can be specified via the CLI or a Pa11y CI configuration file.
Complete details on using reporters can be found in the
[Pa11y CI documentation](https://github.com/pa11y/pa11y-ci#reporters).

### Using via CLI

The reporter can be used with Pa11y CI via the CLI as follows:

```sh
pa11y-ci --reporter=pa11y-ci-reporter-cli-summary https://pa11y.org/
```

When specified via the CLI:

- Only one reporter can be used. If multiple reporters are required, they must
  be specified via a Pa11y CI configuration file.
- Reporter configuration options cannot be specified and the default values are
  used (see [below](#using-via-configuration-file))
- If a reporter is specified via the CLI it overrides the Pa11y CI configuration
  file settings for reporters

### Using via configuration file

You can specify reporters in a Pa11y CI configuration file in the
`defaults.reporters` property (an array).

```json
{
  "defaults": {
    "reporters": ["pa11y-ci-reporter-cli-summary"]
  },
  "urls": ["https://pa11y.org/"]
}
```

The reporter can also be used with one optional configuration option:

- `showPageErrors`: A Boolean value specifying whether page errors are output
  with the Pa11y CI results (default: true). Page errors are cases where Pa11y
  CI fails to execute (for example DNS resolution error, execution timeout). A
  count of the page errors, if any occur, is always shown in the total count.

In this case the reporter is specified as an array, with the first element a
string with the reporter name and the second element an object with the
configuration options.

```json
{
  "defaults": {
    "reporters": [
      ["pa11y-ci-reporter-cli-summary", { "showPageErrors": false }]
    ]
  },
  "urls": ["https://pa11y.org/"]
}
```

Multiple reporters can also be used as shown below:

```json
{
  "defaults": {
    "reporters": [
      "pa11y-ci-reporter-cli-summary",
      ["pa11y-ci-reporter-html", { "destination": "./reports" }]
    ]
  },
  "urls": ["https://pa11y.org/"]
}
```
