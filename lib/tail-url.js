'use strict';

/**
 * Function to shorten a URL.
 *
 * @module tail-url
 */

/**
 * Shortens a URL by identifying the string with the trailing
 * portion of the URL within the maximum length constraint,
 * split at a logical URL boundary if one can be found
 * (e.g. "/", "?", "#"), and prepended by an ellipsis.
 *
 * @param   {string} url       The url to shorten.
 * @param   {number} maxLength The maximum lenth of the returned string.
 * @returns {string}           The shortened url.
 * @static
 */
// eslint-disable-next-line complexity -- allow low threshold
const tailUrl = (url, maxLength) => {
    if (url.length <= maxLength) {
        return url;
    }

    let index = 0;
    // Look for all breaks on logical separators within a URL
    const matches = [...url.matchAll(/[#/?]/g)];
    for (let i = matches.length - 1; i >= 0; i--) {
        const match = matches[i];
        // Try first with matching character (index, +1 for ellipsis)
        if (url.length - match.index + 1 <= maxLength) {
            // eslint-disable-next-line prefer-destructuring -- for consistency
            index = match.index;
        }
        // Try without matching character (-1 for match character, +1 for ellipsis)
        else if (url.length - match.index <= maxLength) {
            index = match.index + 1;
        } else {
            break;
        }
    }

    // No breakpoint was found, so truncate at the max length
    if (index === 0) {
        index = url.length - maxLength + 1;
    }

    return `…${url.slice(index)}`;
};

module.exports = tailUrl;
